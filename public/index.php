<?php
require __DIR__ . '/../vendor/autoload.php';
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>ALGO PROPOSAL</title>
</head>
<body>
<nav class="navbar navbar-light bg-light mb-3">
    <div class="container-fluid">
        <a class="navbar-brand" href="https://algosd.com">algosd</a>
    </div>
</nav>

<div class="container" x-data="dropdown()">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title mb-4">Search Form</h5>
            <div class="row mb-5">
                <div class="col-md-6">
                    <label for="first" class="form-label">First Name</label>
                    <input x-model="firstName" type="text" class="form-control" id="first" placeholder="John">
                </div>
                <div class="col-md-6">
                    <label for="last" class="form-label">Last Name</label>
                    <input x-model="lastName" type="text" class="form-control" id="last" placeholder="Doe">
                </div>
            </div>
            <button @click="search()" type="button" class="btn btn-primary">SEARCH</button>
        </div>
    </div>
</div>

<!-- Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

<!-- Alpine.js -->
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
    function dropdown() {
        return {
            firstName: '',
            lastName: '',
            search() {
                const data = new FormData();
                data.append('first_name', this.firstName);
                data.append('last_name', this.lastName);

                axios
                    .post('/api.php', data)
                    .then(function (response) {
                        // handle success
                        console.log(response.data);
                    });
            },
        }
    }
</script>
</body>
</html>